package httpclients;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpHeaders;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.HttpParams;
import org.apache.http.util.EntityUtils;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.inject.Inject;

public class BitQueryHttpClientImpl {

	@Inject
	public core.Configuration Configuration;
	
	public JsonObject makeAPICall(String json) throws URISyntaxException, ClientProtocolException, IOException {
		String response_content = "";

		URIBuilder query = new URIBuilder(Configuration.GraphqlBitQuery_Url);
		List<NameValuePair> parameters = new ArrayList<NameValuePair>();
		parameters.add(new BasicNameValuePair("query",json));
		query.addParameters(parameters);
		
		CloseableHttpClient client = HttpClients.createDefault();
		HttpPost request = new HttpPost(query.build());

		request.setHeader(HttpHeaders.ACCEPT, "application/json");

		request.addHeader("X-API-KEY", Configuration.GraphqlBitQuery_API_KEY);

		System.out.println("{\"query\": \""+json+"\"}");

		//System.out.println(request.getEntity().));
		CloseableHttpResponse response = client.execute(request);

		try {
			System.out.println(response.getStatusLine());
			
			HttpEntity entity = response.getEntity();
			response_content = EntityUtils.toString(entity);
			EntityUtils.consume(entity);
			System.out.println(response_content);
		} finally {
			response.close();
		}

		JsonObject jsonObject = (JsonObject) JsonParser.parseString(response_content);
		return jsonObject;
	}
}
