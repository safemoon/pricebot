package httpclients;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpHeaders;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;

import com.google.inject.Inject;

import org.apache.http.client.methods.HttpGet;

public class CMCHttpClientImpl {

	@Inject
	public core.Configuration Configuration;

	public CMCHttpClientImpl() {
	}

	
	public String makeAPICall(String uri, List<NameValuePair> parameters) throws URISyntaxException, ClientProtocolException, IOException {
		String response_content = "";

		URIBuilder query = new URIBuilder(Configuration.CMC_API_URL + uri);
		query.addParameters(parameters);

		CloseableHttpClient client = HttpClients.createDefault();
		HttpGet request = new HttpGet(query.build());

		request.setHeader(HttpHeaders.ACCEPT, "application/json");
		request.addHeader("X-CMC_PRO_API_KEY", Configuration.CMC_API_KEY);

		CloseableHttpResponse response = client.execute(request);

		try {
			System.out.println(response.getStatusLine());
			
			HttpEntity entity = response.getEntity();
			response_content = EntityUtils.toString(entity);
			EntityUtils.consume(entity);
			System.out.println(response_content);
		} finally {
			response.close();
		}

		return response_content;
	}

}
