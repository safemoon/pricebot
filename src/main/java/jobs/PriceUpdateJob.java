package jobs;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.Collection;
import java.util.HashMap;

import org.apache.http.client.ClientProtocolException;
import org.quartz.DisallowConcurrentExecution;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import com.google.inject.Inject;
import com.google.inject.name.Named;

import core.PropertyKind;
import core.Startup;
import net.dv8tion.jda.api.JDA;
import net.dv8tion.jda.api.entities.MessageEmbed;
import net.dv8tion.jda.api.entities.channel.concrete.TextChannel;
import net.dv8tion.jda.api.utils.messages.MessageCreateBuilder;
import net.dv8tion.jda.api.utils.messages.MessageCreateData;
import net.dv8tion.jda.api.EmbedBuilder;
import services.CMCService;
import services.DEXSCRNRService;
import services.PriceService;
import services.model.CMC.CMCQuotesLatestResult;
import services.model.CMC.CMCQuotesLatestResultData;
import services.model.CMC.CMCResponse;
import services.model.DEXSCRNR.DEXSCRNRGetPairResponse;

import java.util.*;

@DisallowConcurrentExecution 
public class PriceUpdateJob implements Job {

	@Inject
	private JDA DiscordBot;
	
	@Inject
	public core.Configuration Configuration;
	
	@Inject
	public CMCService _CMCService;
	
	@Inject
	public DEXSCRNRService _DEXSCRNRService;
	
	@Inject
	public PriceService _priceService;
	
	public void execute(JobExecutionContext context) throws JobExecutionException {
		System.out.println("price");
		System.out.println(Configuration.DISCORD_BSC_CHANNEL_NAME);
		TextChannel textChannel = DiscordBot.getTextChannelsByName(Configuration.DISCORD_BSC_CHANNEL_NAME, false).get(0);
		

		try {
			/*CMCResponse<HashMap<String, CMCQuotesLatestResultData>> data = _CMCService.getSFMInfo();
			CMCQuotesLatestResultData sfmData = (CMCQuotesLatestResultData) data.data.values().toArray()[0];
			textChannel.sendMessage(sfmData.quote.get("USD").price + "$").queue();
			
			DEXSCRNRGetPairResponse DEXSCRNRBSCSFMInfos = _DEXSCRNRService.getBSCSFMInfo();
			textChannel.sendMessage(DEXSCRNRBSCSFMInfos.pair.priceUsd + "$").queue();*/
			
			textChannel.sendMessageEmbeds(_priceService.getSFMStatsMessageEmbeded(_priceService.getSFMStats())).queue();

		} catch(Exception e) {
			e.printStackTrace();
		}
	}

}
