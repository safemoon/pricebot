package services;

import java.io.IOException;
import java.lang.reflect.Type;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.message.BasicNameValuePair;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.google.inject.Inject;

import httpclients.CMCHttpClientImpl;
import httpclients.DEXSCRNRHttpClientImpl;
import services.model.CMC.CMCQuotesLatestResultData;
import services.model.CMC.CMCResponse;
import services.model.DEXSCRNR.DEXSCRNRGetPairResponse;

public class DEXSCRNRService {

	@Inject
	public core.Configuration _Configuration;
	
	@Inject
	private DEXSCRNRHttpClientImpl _DEXSCRNRHttpClient;
	
	@Inject
	private Gson _Gson;
	
	
	private String getPairsInfos = "/latest/dex/tokens";
	private String BSC = "/bsc";
	private String ETH = "/eth";
	
	public DEXSCRNRGetPairResponse getBSCSFMInfo() throws ClientProtocolException, URISyntaxException, IOException {
		 List<NameValuePair> parameters = new ArrayList<NameValuePair>();
		 String response = _DEXSCRNRHttpClient.makeAPICall(getPairsInfos+'/'+_Configuration.DEXSCRNR_BSC_SFM_TOKEN_ADDRESS, parameters);
		 DEXSCRNRGetPairResponse result = _Gson.fromJson(response, DEXSCRNRGetPairResponse.class);
		 
		 return result;
	}
	
	public DEXSCRNRGetPairResponse getETHSFMInfo() throws ClientProtocolException, URISyntaxException, IOException {
		 List<NameValuePair> parameters = new ArrayList<NameValuePair>();
		 String response = _DEXSCRNRHttpClient.makeAPICall(getPairsInfos+'/'+_Configuration.DEXSCRNR_ETH_SFM_TOKEN_ADDRESS, parameters);
		 DEXSCRNRGetPairResponse result = _Gson.fromJson(response, DEXSCRNRGetPairResponse.class);
		 
		 return result;
	}
	
	public DEXSCRNRGetPairResponse getPOLYSFMInfo() throws ClientProtocolException, URISyntaxException, IOException {
		 List<NameValuePair> parameters = new ArrayList<NameValuePair>();
		 String response = _DEXSCRNRHttpClient.makeAPICall(getPairsInfos+'/'+_Configuration.DEXSCRNR_POLY_SFM_TOKEN_ADDRESS, parameters);
		 DEXSCRNRGetPairResponse result = _Gson.fromJson(response, DEXSCRNRGetPairResponse.class);
		 
		 return result;
	}
}
