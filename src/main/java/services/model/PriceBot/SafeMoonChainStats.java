package services.model.PriceBot;

import java.math.BigDecimal;
import java.math.BigInteger;

import common.Chain;

public class SafeMoonChainStats{

	public Chain _chain = Chain.NONE;
	
	public SafeMoonChainStats() {
		
	}
	
	protected SafeMoonChainStats(Chain chain) {
		this._chain = chain;
	}
	
	// RAW data from APIs
	public double SFMUSDPrice;
	public double ChangePercent24h;
	public double TotalDEX24hrVolumeUSD;
	public double SWAPDEX24hVolumeUSD;
	public double SWAPLiquidityTotalUSD;
	public double SWAPLiquidityNativeCoin;
	public double SWAPLiquiditySFM;
	public double MarketCap;
	
	// Calculated data
	public BigDecimal CirculatingSupply = new BigDecimal(0);
	public BigDecimal TotalBurn = new BigDecimal(0);
	public double totalBurn24hInUSD;
	public double totalBurn24hInSFM;
	public double DailyReflectionsPerMillionSFMinSFM;
	public double DailyReflectionsPerMillionSFMinUSD;
	public double RemainingBNBSswapEvolve;
	
	public double LiquidityInNativeCoin;

	
}
