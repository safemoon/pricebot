package services.model.PriceBot;

public class SafeMoonGlobalStats{

	public SafeMoonChainStats CrossChainsStats = new SafeMoonChainStats();
	public SafeMoonBSCStats BSCStats = new SafeMoonBSCStats();
	public SafeMoonETHStats ETHStats = new SafeMoonETHStats();
	public SafeMoonPOLYStats POLYStats = new SafeMoonPOLYStats();
	
	public double MarketCap;
	public double BTCUSDPrice;
	public double BNBUSDPrice;
}
