package services.model.DEXSCRNR;

import java.util.ArrayList;
import java.util.HashMap;

public class DEXSCRNRPairInfo {
	public String chainId;
	public String dexId;
	public String url;
	public String pairAddress;
	public ArrayList<String> labels;
	public DEXSCRNRTokenInfo baseToken;
	public DEXSCRNRTokenInfo quoteToken;
	public String priceNative;
	public String priceUsd;
	public HashMap<String, DEXSCRNRTxnInfo> txns;
	public DEXSCRNRVolumeInfo volume;
	public DEXSCRNRPriceChangeInfo priceChange;
	public DEXSCRNRLiquidityInfo liquidity;
	public String fdv;
	public String pairCreatedAt;
}
