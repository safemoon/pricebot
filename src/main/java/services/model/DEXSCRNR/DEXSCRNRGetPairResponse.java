package services.model.DEXSCRNR;

import java.util.ArrayList;
import java.util.List;

public class DEXSCRNRGetPairResponse {
	public String SchemaVersion;
	public List<DEXSCRNRPairInfo> pairs;
	//public DEXSCRNRPairInfo pair;
}
