package services.model.CMC;

public class CMCResponseStatus {
	public String timestamp;
	public String error_code;
	public String error_message;
	public String elapsed;
	public String credit_count;
}
