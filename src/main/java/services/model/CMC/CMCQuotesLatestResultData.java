package services.model.CMC;

import java.util.HashMap;

public class CMCQuotesLatestResultData {
	public String id;
	public String name;
	public String symbol;
	public String slug;
	public String is_active;
	public String is_fiat;
	public String circulating_supply;
	public String max_supply;
	public String date_added;
	public String num_market_pairs;
	public String cmc_rank;
	public String last_updated;
	public HashMap<String, CMCQuoteData> quote;
}
