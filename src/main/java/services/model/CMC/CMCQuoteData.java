package services.model.CMC;

public class CMCQuoteData {
	public String price;
	public String volume_24h;
	public String volume_change_24h;
	public String percent_change_1h;
	public String percent_change_24h;
	public String percent_change_7d;
	public String percent_change_30d;
	public String market_cap;
	public String market_cap_dominance;
	public String fully_diluted_market_cap;
	public String last_updated;
}
