package services;

import java.io.IOException;
import java.math.BigDecimal;
import java.net.URISyntaxException;

import org.apache.http.client.ClientProtocolException;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.inject.Inject;

import common.Chain;
import common.ChainHelper;
import httpclients.BitQueryHttpClientImpl;
import httpclients.CMCHttpClientImpl;

public class BitQueryService {
	
	@Inject
	private ChainHelper chainHelper;
// region Queries
	private String getBSCAllDEXVolume24hrInUSD = "{"
			+ "  ethereum(network: %s) {"
			+ "    dexTrades("
			+ "      options: {limit: 24, desc: \"timeInterval.hour\"}"
			+ "      baseCurrency: {is: \"%s\"}"
			+ "      date: {since: \"2021-06-03\"}"
			+ "    ) {"
			+ "      count"
			+ "      tradeAmount(in: USD)"
			+ "      timeInterval {"
			+ "        hour(count: 1)"
			+ "      }"
			+ "    }"
			+ "  }"
			+ "}";
	
	private final String getBSCAllKnownDEXVolume24hrInUSD = "{"
			+ "  ethereum(network: bsc) {"
			+ "    dexTrades("
			+ "      options: {limit: 1, desc: \"timeInterval.hour\"}"
			+ "      date: {since: \"2022-06-03\"}"
			+ "      baseCurrency: {is: \"%s\"}"
			+ "      smartContractAddress: {}"
			+ "    ) {"
			+ "      count"
			+ "      tradeAmount(in: USD)"
			+ "      timeInterval {"
			+ "        hour(count: 24)"
			+ "      }"
			+ "      exchange {"
			+ "        fullName"
			+ "      }"
			+ "    }"
			+ "  }"
			+ "}";
	
	private final String DeployerBSCBalanceSFM = "{"
			+ "  ethereum(network: %s) {"
			+ "    address(address: {is: \"0x678ee23173dce625A90ED651E91CA5138149F590\"}) {"
			+ "      balances(currency: {is: \"%s\"}) {"
			+ "        value"
			+ "        currency {"
			+ "          symbol"
			+ "        }"
			+ "      }"
			+ "    }"
			+ "  }"
			+ "}";
	
	private final String BurnBSCSFMReceivedSFM = "query MyQuery {"
			+ "  ethereum(network: %s) {"
			+ "    transfers("
			+ "      currency: {is: \"%s\"}"
			+ "      receiver: {is: \"0x0000000000000000000000000000000000000001\"}"
			+ "    ) {"
			+ "      amount"
			+ "    }"
			+ "  }"
			+ "}";
	
	private final String BurnBSCSFMSentSFM = "query MyQuery {"
			+ "  ethereum(network: %s) {"
			+ "    transfers("
			+ "      currency: {is: \"%s\"}"
			+ "      sender: {is: \"0x0000000000000000000000000000000000000001\"}"
			+ "    ) {"
			+ "      amount"
			+ "    }"
			+ "  }"
			+ "}";
	
// endregion
	@Inject
	public core.Configuration _Configuration;
	
	@Inject
	private BitQueryHttpClientImpl _BitQueryHttpClient;
	
	@Inject
	private Gson _Gson;
	
	
	public double getAllDEXVolume24hrUSD(Chain chain) throws ClientProtocolException, URISyntaxException, IOException {
		Double sum = (double) 0;
		for(JsonElement obj : _BitQueryHttpClient.makeAPICall(getBSCAllDEXVolume24hrInUSD.formatted(chainHelper.getBitQueryChainName(chain), chainHelper.getTokenAddress(chain)))
				.get("data").getAsJsonObject()
				.get("ethereum").getAsJsonObject()
				.get("dexTrades").getAsJsonArray()) {
			sum = sum + obj.getAsJsonObject().get("tradeAmount").getAsDouble();
			System.out.println(sum);
		}
		return sum;
	}
	
	public double getSWAPVolume24hrUSD(Chain chain) throws NumberFormatException, ClientProtocolException, URISyntaxException, IOException {

		Double allDEX = getAllDEXVolume24hrUSD(chain);
		
		Double allKnownDEX = (double) 0;
		for(JsonElement obj : _BitQueryHttpClient.makeAPICall(getBSCAllDEXVolume24hrInUSD.formatted(chainHelper.getBitQueryChainName(chain), chainHelper.getTokenAddress(chain)))
				.get("data").getAsJsonObject()
				.get("ethereum").getAsJsonObject()
				.get("dexTrades").getAsJsonArray()) {
			allKnownDEX += obj.getAsJsonObject().get("tradeAmount").getAsDouble();
		}
		
		return allDEX - allKnownDEX;
	}
	
	public BigDecimal getBurn(Chain chain) throws ClientProtocolException, URISyntaxException, IOException {
		BigDecimal BSCburn = _BitQueryHttpClient.makeAPICall(BurnBSCSFMReceivedSFM.formatted(chainHelper.getBitQueryChainName(chain), chainHelper.getTokenAddress(chain)))
							.get("data").getAsJsonObject()
							.get("ethereum").getAsJsonObject()
							.get("transfers").getAsJsonArray().get(0)
							.getAsJsonObject().get("amount")
							.getAsBigDecimal().subtract(
					
						_BitQueryHttpClient.makeAPICall(BurnBSCSFMSentSFM.formatted(chainHelper.getBitQueryChainName(chain), chainHelper.getTokenAddress(chain)))
							.get("data").getAsJsonObject()
							.get("ethereum").getAsJsonObject()
							.get("transfers").getAsJsonArray().get(0)
							.getAsJsonObject().get("amount")
							.getAsBigDecimal()).add(
					
						_BitQueryHttpClient.makeAPICall(DeployerBSCBalanceSFM.formatted(chainHelper.getBitQueryChainName(chain), chainHelper.getTokenAddress(chain)))
							.get("data").getAsJsonObject()
							.get("ethereum").getAsJsonObject()
							.get("address").getAsJsonArray().get(0)
							.getAsJsonObject().get("balances").getAsJsonArray().get(0)
							.getAsJsonObject().get("value")
							.getAsBigDecimal());
		return BSCburn;
		
	}
	
	
}
