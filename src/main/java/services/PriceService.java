package services;

import java.awt.Color;
import java.util.*;
import java.util.spi.CurrencyNameProvider;
import java.util.stream.*;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.net.URISyntaxException;
import java.text.NumberFormat;

import org.apache.http.client.ClientProtocolException;

import com.google.inject.Inject;

import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.entities.MessageEmbed;
import net.dv8tion.jda.api.entities.MessageEmbed.Field;
import net.dv8tion.jda.internal.utils.tuple.MutablePair;
import net.dv8tion.jda.internal.utils.tuple.Pair;
import services.model.DEXSCRNR.DEXSCRNRGetPairResponse;
import services.model.DEXSCRNR.DEXSCRNRPairInfo;
import services.model.PriceBot.SafeMoonChainStats;
import services.model.PriceBot.SafeMoonGlobalStats;

public class PriceService {

	@Inject
	public core.Configuration Configuration;

	@Inject
	public CMCService _CMCService;

	@Inject
	public DEXSCRNRService _DEXSCRNRService;

	@Inject
	public BitQueryService _BitQueryService;

	public SafeMoonGlobalStats getSFMStats() throws ClientProtocolException, URISyntaxException, IOException {
		SafeMoonGlobalStats globalStats = new SafeMoonGlobalStats();

		List<Pair<DEXSCRNRGetPairResponse, SafeMoonChainStats>> DexScreenerToChainStats = new ArrayList<Pair<DEXSCRNRGetPairResponse, SafeMoonChainStats>>();
		DexScreenerToChainStats.add(new MutablePair<DEXSCRNRGetPairResponse, SafeMoonChainStats>(
				_DEXSCRNRService.getBSCSFMInfo(), globalStats.BSCStats));
		DexScreenerToChainStats.add(new MutablePair<DEXSCRNRGetPairResponse, SafeMoonChainStats>(
				_DEXSCRNRService.getETHSFMInfo(), globalStats.ETHStats));
		DexScreenerToChainStats.add(new MutablePair<DEXSCRNRGetPairResponse, SafeMoonChainStats>(
				_DEXSCRNRService.getPOLYSFMInfo(), globalStats.POLYStats));

		for (Pair<DEXSCRNRGetPairResponse, SafeMoonChainStats> pair : DexScreenerToChainStats) {
			SafeMoonChainStats safeMoonChainStats = pair.getRight();
			try {
				// Main info comes from DexScreener
				DEXSCRNRGetPairResponse DEXSCRNRBSCSFMInfos = pair.getLeft();

				DEXSCRNRPairInfo DEXSCRNRBSCSFMSWAPInfo = DEXSCRNRBSCSFMInfos.pairs.stream()
						.filter(p -> p.dexId.equals("safemoonswap")).collect(Collectors.toList()).get(0);

				safeMoonChainStats.SFMUSDPrice = Double.parseDouble(DEXSCRNRBSCSFMSWAPInfo.priceUsd);
				safeMoonChainStats.ChangePercent24h = Double.parseDouble(DEXSCRNRBSCSFMSWAPInfo.priceChange.h24);
				safeMoonChainStats.SWAPDEX24hVolumeUSD = Double.parseDouble(DEXSCRNRBSCSFMSWAPInfo.volume.h24);
				safeMoonChainStats.TotalDEX24hrVolumeUSD = DEXSCRNRBSCSFMInfos.pairs.stream()
						.mapToDouble(p -> Double.parseDouble(p.volume.h24)).sum();
				safeMoonChainStats.SWAPLiquidityTotalUSD = Double.parseDouble(DEXSCRNRBSCSFMSWAPInfo.liquidity.usd);
				safeMoonChainStats.SWAPLiquidityNativeCoin = Double.parseDouble(DEXSCRNRBSCSFMSWAPInfo.liquidity.quote);
				safeMoonChainStats.SWAPLiquiditySFM = Double.parseDouble(DEXSCRNRBSCSFMSWAPInfo.liquidity.base);
				//safeMoonChainStats.MarketCap = Double.parseDouble(DEXSCRNRBSCSFMSWAPInfo.fdv);
				
				safeMoonChainStats.ChangePercent24h = Double.parseDouble(DEXSCRNRBSCSFMSWAPInfo.priceChange.h24);
				safeMoonChainStats.totalBurn24hInUSD = safeMoonChainStats.TotalDEX24hrVolumeUSD * Configuration.SFMburn;
				safeMoonChainStats.totalBurn24hInSFM = safeMoonChainStats.totalBurn24hInUSD
						/ safeMoonChainStats.SFMUSDPrice;

				// Other info gathered somewhere else
				double chainTotalBurn;
				double chainCirculatingSupply;
				double chainRemainingSwapEvolve;
				
				safeMoonChainStats.TotalBurn = _BitQueryService.getBurn(safeMoonChainStats._chain);
				BigDecimal e = new BigDecimal("1000000000000");
				safeMoonChainStats.CirculatingSupply = new BigDecimal("1000000000000").subtract(safeMoonChainStats.TotalBurn);
				
				safeMoonChainStats.MarketCap = safeMoonChainStats.CirculatingSupply.multiply(new BigDecimal(safeMoonChainStats.SFMUSDPrice)).doubleValue();
				safeMoonChainStats.DailyReflectionsPerMillionSFMinUSD = (safeMoonChainStats.TotalDEX24hrVolumeUSD
						* 0.04)
						* (((new BigDecimal(1000000)).divide(safeMoonChainStats.CirculatingSupply, 10,
								RoundingMode.HALF_UP)).doubleValue());
				safeMoonChainStats.DailyReflectionsPerMillionSFMinSFM = safeMoonChainStats.DailyReflectionsPerMillionSFMinUSD
						/ safeMoonChainStats.SFMUSDPrice;
				System.out.println(((new BigDecimal(1000000)).divide(safeMoonChainStats.CirculatingSupply, 7,
						RoundingMode.HALF_UP)));
				
				
				SafeMoonChainStats crossChainStats = globalStats.CrossChainsStats;
				crossChainStats.totalBurn24hInSFM += safeMoonChainStats.totalBurn24hInSFM;
				crossChainStats.SWAPLiquidityTotalUSD += safeMoonChainStats.SWAPLiquidityTotalUSD;
				crossChainStats.MarketCap += safeMoonChainStats.MarketCap;
				crossChainStats.CirculatingSupply = crossChainStats.CirculatingSupply.add(safeMoonChainStats.CirculatingSupply);
				crossChainStats.TotalDEX24hrVolumeUSD += safeMoonChainStats.TotalDEX24hrVolumeUSD;
			} catch (Exception e) {
				System.out.println("error retrieving data for " + safeMoonChainStats._chain);
				e.printStackTrace();
			}
		}

		return globalStats;
	}

	public MessageEmbed getSFMStatsMessageEmbeded(SafeMoonGlobalStats globalPriceStats) {
		MessageEmbed message = new EmbedBuilder().setAuthor("Mahna Mahna Doo Doo Da Doo Doo")
				.setTitle("🔥 All Stats - V2 🔥").setColor(Color.decode("#3B9BD7"))
				.addField(new Field("<:sfm:824031099402321961> SWAP V3 Price (SFS BNB)",
						"BSC : $" + (String.format("%,7f", globalPriceStats.BSCStats.SFMUSDPrice))
						+"\nETH : $" + (String.format("%,7f", globalPriceStats.ETHStats.SFMUSDPrice))
						+"\nPOLY : $" + (String.format("%,7f", globalPriceStats.POLYStats.SFMUSDPrice)), true))
				.addField(new Field("🔥 24 Hour SWAP Burn",
						"\nTotal : "+String.format("%,.2f", globalPriceStats.CrossChainsStats.totalBurn24hInSFM) + " SFM"
						+"\nBSC : "+String.format("%,.2f", globalPriceStats.BSCStats.totalBurn24hInSFM) + " SFM"
						+"\nETH : "+String.format("%,.2f", globalPriceStats.ETHStats.totalBurn24hInSFM) + " SFM"
						+"\nPOLY : "+String.format("%,.2f", globalPriceStats.POLYStats.totalBurn24hInSFM) + " SFM", true))
				.addField(
						new Field("💥 Total DEX Volume",
								"Total : "+currencyWithChosenLocalisation(globalPriceStats.CrossChainsStats.TotalDEX24hrVolumeUSD,
										Locale.US)
								+"\nBSC : "+currencyWithChosenLocalisation(globalPriceStats.BSCStats.TotalDEX24hrVolumeUSD,
										Locale.US)
								+"\nETH : "+currencyWithChosenLocalisation(globalPriceStats.ETHStats.TotalDEX24hrVolumeUSD,
										Locale.US)
								+"\nPOLY : "+currencyWithChosenLocalisation(globalPriceStats.POLYStats.TotalDEX24hrVolumeUSD,
										Locale.US),
								true))
				.addField(new Field("🏛️ MarketCap",
						"Total : "+currencyWithChosenLocalisation(globalPriceStats.CrossChainsStats.MarketCap, Locale.US)
						+"\nBSC : "+currencyWithChosenLocalisation(globalPriceStats.BSCStats.MarketCap, Locale.US)
						+"\nETH : "+currencyWithChosenLocalisation(globalPriceStats.ETHStats.MarketCap, Locale.US)
						+"\nPOLY : "+currencyWithChosenLocalisation(globalPriceStats.POLYStats.MarketCap, Locale.US), true))
				.addField(new Field("Circulating Supply & Burn Wallet",
						"Current Circulating supply : "
						+ String.format("%,.2f", globalPriceStats.CrossChainsStats.CirculatingSupply) + " SFM"
						+"\nBSC :\nBurn Wallet + Deployer : " + String.format("%,.2f", globalPriceStats.BSCStats.TotalBurn)
								+ " SFM\n" + "Circulating supply : "
								+ String.format("%,.2f", globalPriceStats.BSCStats.CirculatingSupply) + " SFM"
						+"\nETH :\nBurn Wallet + Deployer : " + String.format("%,.2f", globalPriceStats.ETHStats.TotalBurn)
						+ " SFM\n" + "Circulating supply : "
						+ String.format("%,.2f", globalPriceStats.ETHStats.CirculatingSupply) + " SFM"
						+"\nPOLY :\nBurn Wallet + Deployer : " + String.format("%,.2f", globalPriceStats.POLYStats.TotalBurn)
						+ " SFM\n" + "Circulating supply : "
						+ String.format("%,.2f", globalPriceStats.POLYStats.CirculatingSupply) + " SFM",
						true))
				.addField(new Field("💰 SWAP Reflections (Per Million Tokens - Estimate)",
						"BSC : "+String.format("%,.2f", globalPriceStats.BSCStats.DailyReflectionsPerMillionSFMinSFM) + " SFM"
								+ " ("
								+ currencyWithChosenLocalisation(
										globalPriceStats.BSCStats.DailyReflectionsPerMillionSFMinUSD, Locale.US)
								+ ")"
						+"\n ETH : "+String.format("%,.2f", globalPriceStats.ETHStats.DailyReflectionsPerMillionSFMinSFM) + " SFM"
						+ " ("
						+ currencyWithChosenLocalisation(
								globalPriceStats.ETHStats.DailyReflectionsPerMillionSFMinUSD, Locale.US)
						+ ")"
						+"\n POLY : "+String.format("%,.2f", globalPriceStats.POLYStats.DailyReflectionsPerMillionSFMinSFM) + " SFM"
						+ " ("
						+ currencyWithChosenLocalisation(
								globalPriceStats.POLYStats.DailyReflectionsPerMillionSFMinUSD, Locale.US)
						+ ")",
						true))
				.addField(new Field("SWAP Liquidity",
						"Total : "
								+ currencyWithChosenLocalisation(globalPriceStats.CrossChainsStats.SWAPLiquidityTotalUSD,
										Locale.US)
								+ "\nBSC :\n" + String.format("%,.2f", globalPriceStats.BSCStats.SWAPLiquidityNativeCoin)
								+ " BNB\n" + String.format("%,.2f", globalPriceStats.BSCStats.SWAPLiquiditySFM)
								+ " SFM"
								+ "\nETH :\n" + String.format("%,.2f", globalPriceStats.ETHStats.SWAPLiquidityNativeCoin)
								+ " ETH\n" + String.format("%,.2f", globalPriceStats.ETHStats.SWAPLiquiditySFM)
								+ " SFM"
								+ "\nPOLY :\n" + String.format("%,.2f", globalPriceStats.POLYStats.SWAPLiquidityNativeCoin)
								+ " MATIC\n" + String.format("%,.2f", globalPriceStats.POLYStats.SWAPLiquiditySFM)
								+ " SFM",
						true))
				.addField(new Field("<:uptrend:838343716276142080> 24HR Percentage Change",
						"BSC :"+String.valueOf(globalPriceStats.BSCStats.ChangePercent24h) + " %"
						+"\nETH :"+String.valueOf(globalPriceStats.ETHStats.ChangePercent24h) + " %"
						+"\nPOLY :"+String.valueOf(globalPriceStats.POLYStats.ChangePercent24h) + " %", true))
				.addField(new Field("<:pandaBags:840623325909745674> Remaining BNB For Swap & Evolve",
						String.valueOf(0), true))
				.addField(new Field("Other Coins", String.valueOf(0), true)).build();
		return message;
	}

	public static String currencyWithChosenLocalisation(Object value, Locale locale) {
		NumberFormat nf = NumberFormat.getCurrencyInstance(locale);
		return nf.format(value);

	}

}
