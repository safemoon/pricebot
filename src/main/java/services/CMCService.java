package services;

import com.google.gson.Gson;
import com.google.inject.Inject;

import httpclients.CMCHttpClientImpl;
import services.model.CMC.CMCQuotesLatestResultData;
import services.model.CMC.CMCResponse;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import org.apache.http.message.BasicNameValuePair;

import java.lang.reflect.Type;
import com.google.gson.reflect.TypeToken;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;

public class CMCService {

	@Inject
	public core.Configuration _Configuration;
	
	@Inject
	private CMCHttpClientImpl _CMCHttpClient;
	
	@Inject
	private Gson _Gson;
	
	
	private String quotesLatest = "/v2/cryptocurrency/quotes/latest";
	
	public CMCResponse<HashMap<String, CMCQuotesLatestResultData>> getSFMInfo() throws ClientProtocolException, URISyntaxException, IOException {
		 List<NameValuePair> parameters = new ArrayList<NameValuePair>();
		 parameters.add(new BasicNameValuePair("id",_Configuration.CMC_SFM_ID));
		 String response = _CMCHttpClient.makeAPICall(quotesLatest, parameters);
		 TypeToken<CMCResponse<HashMap<String, CMCQuotesLatestResultData>>> obj = new TypeToken<CMCResponse<HashMap<String, CMCQuotesLatestResultData>>>() {};
		 Type t = obj.getType();
		 CMCResponse<HashMap<String, CMCQuotesLatestResultData>> result = _Gson.fromJson(response, t);
		 
		 return result;
	}
}
