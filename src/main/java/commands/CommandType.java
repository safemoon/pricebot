package commands;

public enum CommandType {
	setBSCChannel("setbscchan", "Set SBC channel"),
	setETHChannel("setethchan", "Set ETH channel");
	
	private String _command;
	private String _description;
	
	CommandType(String command, String description) {
		this._command = command;
		this._description = description;
	}
	
	public String getCommandString() {
		return this._command;
	}
	
	public String getDescriptionString() {
		return this._description;
	}
}
