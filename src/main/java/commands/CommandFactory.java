package commands;

import kotlin.NotImplementedError;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.events.interaction.command.SlashCommandInteractionEvent;

public abstract class CommandFactory {
	
	public static ACommand buildCommand(SlashCommandInteractionEvent event) {
		ACommand command = null;
		if(event.getName().equals(CommandType.setBSCChannel.getCommandString())) {
			command = new SetBSCChannelCommand();
		}
		
		if(command == null) {
			throw new NotImplementedError();
		}
		
		return command;
	}

}
