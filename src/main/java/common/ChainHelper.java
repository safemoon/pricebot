package common;

import com.google.inject.Inject;

public class ChainHelper {
	@Inject
	public core.Configuration Configuration;
	
	public String getBitQueryChainName(Chain chain) {
		switch(chain) {
		case BSC:
			return "bsc";
		case ETH:
			return "ethereum";
		case POLY:
			return "matic";
		default:
			return null;
		}
	}
	
	public String getTokenAddress(Chain chain) {
		switch(chain) {
		case BSC:
			return Configuration.DEXSCRNR_BSC_SFM_TOKEN_ADDRESS;
		case ETH:
			return Configuration.DEXSCRNR_ETH_SFM_TOKEN_ADDRESS;
		case POLY:
			return Configuration.DEXSCRNR_POLY_SFM_TOKEN_ADDRESS;
		default:
			return null;
		}
	}
}
