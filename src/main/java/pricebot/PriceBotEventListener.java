package pricebot;
import commands.ACommand;
import commands.CommandFactory;
import commands.CommandType;
import commands.SetBSCChannelCommand;
import net.dv8tion.jda.api.events.interaction.command.SlashCommandInteractionEvent;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;
import net.dv8tion.jda.internal.JDAImpl;

public class PriceBotEventListener extends ListenerAdapter{

	public void onSlashCommandInteraction(SlashCommandInteractionEvent event) {
		System.out.println("event received : "+event.getName());
		ACommand command = CommandFactory.buildCommand(event);
		
		command.execute(event);
	}
}
