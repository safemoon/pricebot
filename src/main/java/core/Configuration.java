package core;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.InvalidPropertiesFormatException;
import java.util.Properties;

public class Configuration {

	public String DISCORD_BOT_API_KEY;
	public String DISCORD_BSC_CHANNEL_NAME;
	public String DISCORD_ETH_CHANNEL_NAME;
	public int PRICE_UPDATE_BSC_SECONDS;
	public String CMC_API_KEY;
	public String CMC_API_URL;
	public String CMC_SFM_ID;
	public String DEXSCRNR_BSC_SFM_TOKEN_ADDRESS = "0x42981d0bfbaf196529376ee702f2a9eb9092fcb5";
	public String DEXSCRNR_ETH_SFM_TOKEN_ADDRESS = "0xe574c0c33a7a67d9b09f9f0addbb3dca71a8f3e0";
	public String DEXSCRNR_POLY_SFM_TOKEN_ADDRESS = "0x0a654cb371FBC3C49Ccb176984D64E86fc931EeA";
	public String DEXSCRNR_API_URL;
	public String GraphqlBitQuery_Url = "https://graphql.bitquery.io";
	public String GraphqlBitQuery_API_KEY = "BQYC2oxSa928JIfK6IOd6NWXWjJ1GXGa";
	
	public double SFMburn = 0.0025;
	
	public Configuration(String configFilePath) throws InvalidPropertiesFormatException, FileNotFoundException, IOException {
		Properties properties = new Properties();
		properties.loadFromXML(new FileInputStream(configFilePath));
		
		DISCORD_BOT_API_KEY = properties.getProperty(PropertyKind.DISCORD_BOT_API_KEY.toString());
		DISCORD_BSC_CHANNEL_NAME = properties.getProperty(PropertyKind.DISCORD_BSC_CHANNEL_NAME.toString());
		DISCORD_ETH_CHANNEL_NAME = properties.getProperty(PropertyKind.DISCORD_ETH_CHANNEL_NAME.toString());
		PRICE_UPDATE_BSC_SECONDS = Integer.parseInt(properties.getProperty(PropertyKind.PRICE_UPDATE_BSC_SECONDS.toString()));
		CMC_API_KEY = properties.getProperty(PropertyKind.CMC_API_KEY.toString());
		CMC_API_URL = properties.getProperty(PropertyKind.CMC_API_URL.toString());
		CMC_SFM_ID = properties.getProperty(PropertyKind.CMC_SFM_ID.toString());
		DEXSCRNR_API_URL = properties.getProperty(PropertyKind.DEXSCRNR_API_URL.toString());
		
	}
}
