package core;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.InvalidPropertiesFormatException;
import java.util.Properties;

import org.quartz.spi.JobFactory;
import org.quartz.JobBuilder;
import org.quartz.JobDetail;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.SimpleScheduleBuilder;
import org.quartz.Trigger;
import org.quartz.TriggerBuilder;
import org.quartz.impl.StdSchedulerFactory;

import com.google.gson.Gson;
import com.google.inject.AbstractModule;
import com.google.inject.name.Names;

import commands.CommandType;
import common.ChainHelper;
import httpclients.*;
import jobs.PriceUpdateJob;
import net.dv8tion.jda.api.JDA;
import net.dv8tion.jda.api.JDABuilder;
import net.dv8tion.jda.api.interactions.commands.build.Commands;
import pricebot.PriceBotEventListener;
import services.*;
import jobs.*;

public class Modules extends AbstractModule{

	private Configuration _configuration = null;
	
	@Override
	protected void configure() {
		try {
			configureConfig();
			configureGson();
			configureHttpClients();
			configureServices();
			configureDiscordBot();
			configureQuartz();
			configureHelpers();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	private void configureConfig() throws Exception {
		String rootPath = Thread.currentThread().getContextClassLoader().getResource("").getPath();
		System.out.println(rootPath);
		String XMLconfigFilePath = rootPath + "config.xml";
		
		_configuration = new Configuration(XMLconfigFilePath);
		bind(Configuration.class).toInstance(_configuration);
	}
	
	private void configureDiscordBot() throws InterruptedException {
		Configuration configuration = _configuration;
		JDA DiscordBot = JDABuilder.createDefault(configuration.DISCORD_BOT_API_KEY)
							.addEventListeners(new PriceBotEventListener())
							.build();
		
		DiscordBot.updateCommands().addCommands(
				Commands.slash(CommandType.setBSCChannel.getCommandString(), CommandType.setBSCChannel.getDescriptionString())
			).queue();
		DiscordBot.awaitReady();
		
		bind(JDA.class).toInstance(DiscordBot);
	}
	
	private void configureQuartz() throws SchedulerException {
		bind(JobFactory.class)
        .to(JobFactoryImpl.class);
		
		Configuration configuration = _configuration;
		StdSchedulerFactory schedFact = new org.quartz.impl.StdSchedulerFactory();
		Scheduler scheduler = schedFact.getScheduler();
		
		// price-update Job
		JobDetail jobDetailsPriceUpdate = JobBuilder.newJob(PriceUpdateJob.class).withIdentity("priceUpdateJob", "price-bot-group").build();
		
		Trigger triggerPriceUpdate = TriggerBuilder.newTrigger().withIdentity("priceUpdateTrigger", "price-bot-group")
														.withSchedule(SimpleScheduleBuilder.simpleSchedule().withIntervalInSeconds(configuration.PRICE_UPDATE_BSC_SECONDS).repeatForever())
														.startNow()
														.build();
		
		scheduler.scheduleJob(jobDetailsPriceUpdate, triggerPriceUpdate);
	}
	
	private void configureGson() {
		Gson Gson = new Gson();
		bind(Gson.class).toInstance(Gson);
	}
	
	private void configureHttpClients() {
		CMCHttpClientImpl CMCHttpClient = new CMCHttpClientImpl();
		bind(CMCHttpClientImpl.class).toInstance(CMCHttpClient);
		
		DEXSCRNRHttpClientImpl DEXSCRNRHttpClient = new DEXSCRNRHttpClientImpl();
		bind(DEXSCRNRHttpClientImpl.class).toInstance(DEXSCRNRHttpClient);
		
		BitQueryHttpClientImpl bitQueryHttpClientImpl = new BitQueryHttpClientImpl();
		bind(BitQueryHttpClientImpl.class).toInstance(bitQueryHttpClientImpl);
	}
	
	private void configureServices() {
		CMCService CMCService = new CMCService();
		bind(CMCService.class).toInstance(CMCService);
		
		DEXSCRNRService DEXSCRNRService = new DEXSCRNRService();
		bind(DEXSCRNRService.class).toInstance(DEXSCRNRService);
		
		BitQueryService BitQueryService = new BitQueryService();
		bind(BitQueryService.class).toInstance(BitQueryService);
		
		PriceService priceService = new PriceService();
		bind(PriceService.class).toInstance(priceService);
		
		
	}
	
	private void configureHelpers() {
		ChainHelper chainHelper = new ChainHelper();
		bind(ChainHelper.class).toInstance(chainHelper);
	}
}
