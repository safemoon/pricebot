package core;

public enum PropertyKind {
	DISCORD_BOT_API_KEY,
	DISCORD_BSC_CHANNEL_NAME,
	DISCORD_ETH_CHANNEL_NAME,
	PRICE_UPDATE_BSC_SECONDS,
	CMC_API_KEY,
	CMC_API_URL,
	CMC_SFM_ID,
	DEXSCRNR_API_URL,
	DEXSCRNR_BSC_SFM_PAIR_ADDRESS,
	DEXSCRNR_ETH_SFM_PAIR_ADDRESS,
	DEXSCRNR_POLY_SFM_PAIR_ADDRESS
}
