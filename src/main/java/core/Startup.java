package core;
import org.quartz.JobBuilder;
import org.quartz.JobDetail;
import org.quartz.ScheduleBuilder;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.SimpleScheduleBuilder;
import org.quartz.Trigger;
import org.quartz.TriggerBuilder;
import org.quartz.impl.StdSchedulerFactory;
import org.quartz.spi.JobFactory;

import com.google.inject.Guice;
import com.google.inject.Inject;
import com.google.inject.Injector;

import commands.CommandType;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.InvalidPropertiesFormatException;
import java.util.Properties;

import jobs.PriceUpdateJob;
import net.dv8tion.jda.api.AccountType;
import net.dv8tion.jda.api.JDA;
import net.dv8tion.jda.api.JDABuilder;
import net.dv8tion.jda.api.interactions.commands.build.Commands;
import pricebot.PriceBotEventListener;

public class Startup {
	
	public static JDA DiscordBot;
	
	@Inject private JobFactory jobFactory;
	
	public static void main(String[] args) {
		Startup app = new Startup();
		try {
			app.initQuartz();
		} catch (SchedulerException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	

	public Startup() {
		inject(this);
	}

    private void inject(Startup app) {
        Injector guice = Guice.createInjector(new Modules());
        guice.injectMembers(app);
    }
    
	private void initQuartz() throws SchedulerException {
		// create schedule with quartz.properties
        Scheduler scheduler = StdSchedulerFactory.getDefaultScheduler();
        // add custom guice injected job factory
        scheduler.setJobFactory(jobFactory);
		
		scheduler.start();
	}
}
